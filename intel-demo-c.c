#include <string.h>

extern void fort_print(char * arg_string);	/* Fortran routine to be called */

/* Receive a string from Fortran, in C format (NULL-terminated), alter it, then
 * pass it to another Fortran routine, in this case to simply print it. */
void c_append (char * arg_string)
{
        char string_buf[100];

	/* NB: Security implications are being glossed over here. */
        strcpy(string_buf, arg_string);
        strcat(string_buf, " interoperates with C");

        /* Call Fortran routine passing modified arg_string */
        fort_print (string_buf);

        return;
}
