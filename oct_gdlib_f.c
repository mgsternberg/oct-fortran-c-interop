/*
 Copyright (C) 2002-2006 the octopus team

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA.

*/

/* #include <config.h>
 * minimally unrolled here:
 */
#define FC_FUNC_(name,NAME) name ## _
#define PACKAGE_BUGREPORT "octopus-devel@tddft.org"

#include <ctype.h>
#include <string.h>
#include <assert.h>

/* #include "string_f.h" --- Fortran <-> C string compatibility issues */

/* #include <gd.h> */
#define gdImagePtr  long *

/* instrumentation */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/* ---------------------- Interface to GD functions ------------------------ */
gdImagePtr oct_gdimage_create_from
  (char * name)
{
  char name_c[PATH_MAX];
  char *ext;
  gdImagePtr im;

  printf("ENTER oct_gdimage_create_from(name %%p = %p ...)\n", name);
  assert(name != NULL);
  strncpy(name_c, name, sizeof(name_c));
  name_c[sizeof(name_c) - 1] = '\0';
  printf("name_c\t'%s'\n", name_c);

  /* locate filename extension, lowercase in situ */
  for(ext=name_c+strlen(name_c); *ext!='.' && ext>=name_c; ext--){
    *ext = tolower(*ext);
  }
  if(ext < name_c || ext == name_c+strlen(name_c)) {
    return NULL; /* could not find file type */
  }

  /* skip '.' before extension */
  ext++;
  printf("ext\t%s\n", ext);

  /* ... */
  if (!(im = malloc(10 * sizeof(long)))) {
      fprintf(stderr, "oct_gdimage_create_from: malloc() failed\n");
      exit(1);
  }
  printf("RETURN oct_gdimage_create_from() = %p\n", im);
  im[0] = 424242424242L;
  return im;
}
