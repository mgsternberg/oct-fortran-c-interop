/*
 Copyright (C) 2003 M. Marques, A. Castro, A. Rubio, G. Bertsch

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA.

 $Id$
*/

/* --------------------- Fortran to C string compatibility ---------------------- */
#include <stdlib.h>

#define to_c_str(f, c, l) {		   \
  int i, ll;                               \
  ll = (int)l;                             \
  for(ll--; ll>=0; ll--)                   \
    if(f[ll] != ' ') break;                \
  ll++;                                    \
  c = (char *)malloc((ll+1)*sizeof(char)); \
  for(i=0; i<ll; i++) c[i] = f[i];         \
  c[i] = '\0';                             \
}

#define to_f_str(c, f, l) {                \
  int i, ll;                               \
  ll = (int)l;                             \
  for(i=0; i<ll && c[i]!='\0'; i++)        \
    f[i] = c[i];                           \
  for(; i<ll; i++)                         \
    f[i] = ' ';                            \
}


#define STR_F_TYPE char *
#define TO_C_STR1(s, c) to_c_str(s, c, l1)
#define TO_C_STR2(s, c) to_c_str(s, c, l2)
#define TO_C_STR3(s, c) to_c_str(s, c, l3)
#define TO_F_STR1(c, f) to_f_str(c, f, l1)
#define TO_F_STR2(c, f) to_f_str(c, f, l2)
#define TO_F_STR3(c, f) to_f_str(c, f, l3)
#define STR_ARG1     , unsigned long l1
#define STR_ARG2     , unsigned long l1, unsigned long l2
#define STR_ARG3     , unsigned long l1, unsigned long l2, unsigned long l3
