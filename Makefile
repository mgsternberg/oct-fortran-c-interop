CFLAGS = -I.
ifdef intel
  CC = icc
  FC = ifort
  FFLAGS = -std03
else
  CC = gcc
  FC = gfortran
  FFLAGS = -std=f2003
endif

%.o: %.F90
	$(FC) $(FFLAGS) -c $<

all: all-oct all-demo

all-oct:
	-$(MAKE) gnu=1   sep clean oct
	-$(MAKE) intel=1 sep clean oct

all-demo:
	-$(MAKE) gnu=1   sep clean demo
	-$(MAKE) intel=1 sep clean demo


SEP = --------------------------------
demo: intel-demo-f.o intel-demo-c.o
	-@echo -e "\n$(SEP)\nDemo code, $(FC)\n$(SEP)\n"
	$(FC) -o $@ $^
	./$@

oct: simul_box.o oct_gdlib_f.o
	-@echo -e "\n$(SEP)\nOctopus isolated code, $(FC)\n$(SEP)\n"
	$(FC) --version
	$(FC) -o $@ $^
	./$@

.PHONY: sep clean
sep:
	-@echo -e "\n$(SEP)\n"
clean:
	rm -f *.o *.mod a.out oct demo
