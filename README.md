## Introduction

The
[Octopus](https://octopus-code.org/wiki/Main_Page)
[9.1](https://octopus-code.org/wiki/Octopus_9)
TD-DFT code, when compiled by the _Intel Fortran/C_ compilers,
fails to read PNG files.

__Reference:__ [Octopus Issue #236](https://gitlab.com/octopus-code/octopus/issues/236)

In the testsuite, at test `finite_systems_2d/08-gdlib`, a segmentation fault
occurs:
```
 ***** gdlib: particle in an octopus *****  

 Using workdir    : /tmp/octopus-.-finite_systems_2d-08-gdlib.test.oIK8eH
 Using test file  : ./finite_systems_2d/08-gdlib.test 

 Using input file : ./finite_systems_2d/08-gdlib.01-gs.inp
 Executing: cd /tmp/octopus-.-finite_systems_2d-08-gdlib.test.oIK8eH; /usr/bin/nice /home/.../octopus-9.1/testsuite/../src/octopus > out 

 forrtl: severe (174): SIGSEGV, segmentation fault occurred
 Image              PC                Routine            Line        Source             
 libifcoremt.so.5   00002B343C442522  for__signal_handl     Unknown  Unknown
 libpthread-2.17.s  00002B343E48C5D0  Unknown               Unknown  Unknown
 octopus            000000000145A66B  Unknown               Unknown  Unknown
 octopus            000000000076B73C  Unknown               Unknown  Unknown
...
```

The fault occurs in the following files of the Octopus source tree:
```
./src/ions/simul_box.F90
./src/basic/oct_gdlib_f.c
```
and **one or both** of the following nearly -- but not entirely(!) -- identical files:
```
./liboct_parser/string_f.h
./external_libs/fortrancl/string_f.h
```

The fault is due to difficulties in portably passing a string variable from
Fortran to C. In the testsuite, the string contains the name of a PNG file to
load. The string arrives on the C side as a pointer with an inconsistent
address, including possibly as a null pointer:
```
$ gdb .../octopus
...
Program received signal SIGSEGV, Segmentation fault.
oct_gdimage_create_from_ (name=0x0, l1=28258056) at basic/oct_gdlib_f.c:40
40        TO_C_STR1(name, name_c);
```

This project contains code snippets from the Octopus code base to isolate and
reproduce the failure, as well as demo code provided by Intel to demonstrate
passing strings from Fortran to C and back.

The Intel demo code offers two ways of passing a string from C to Fortran
(not immediately relevant for this Octopus issue),
where, alas, only _one of them works as-is_ under GNU Fortran.

### Analysis

To handle string passing, Octopus uses a Fortran module and several C
preprocessor macros that attempt to mask yet handle a string-length parameter
that is implicitly passed by Fortran.
This approach looks forced, likely for historical reasons.

Fortran-C interoperability has eased somewhat with Fortran 2003/2008,
but handling strings remains difficult, see e.g.
[an August 2019 forum article](https://software.intel.com/en-us/forums/intel-fortran-compiler/topic/815601).

Intel provides
[demo code](https://software.intel.com/en-us/fortran-compiler-developer-guide-and-reference-characters)
for passing and modifying strings between Fortran and C, involving on the
Fortran side the construct `bind(C)` and `character(len=1) dimension(*)`
instead of regular strings.

The current `Makefile` provides a convenient means to compile and run:

* Octopus isolated code, using GNU compilers,
* Octopus isolated code, using Intel compilers,
* Demo code, using GNU compilers,
* Demo code, using Intel compilers.


## Steps to reproduce

Platform tested: CentOS-7

### Octopus bug (on isolated code)
```
$ make all-oct
```
Output:
```
make gnu=1   sep clean oct
make[1]: Entering directory `/home/SOFT/octopus/fortran-c-interop'

--------------------------------

rm -f *.o *.mod a.out oct demo
gfortran -std=f2003 -c simul_box.F90
gcc -I.   -c -o oct_gdlib_f.o oct_gdlib_f.c

--------------------------------
Octopus isolated code, gfortran
--------------------------------

gfortran --version
GNU Fortran (GCC) 4.8.5 20150623 (Red Hat 4.8.5-36)
Copyright (C) 2015 Free Software Foundation, Inc.

GNU Fortran comes with NO WARRANTY, to the extent permitted by law.
You may redistribute copies of GNU Fortran
under the terms of the GNU General Public License.
For more information about these matters, see the file named COPYING

gfortran -o oct simul_box.o oct_gdlib_f.o
./oct
ENTER oct_gdimage_create_from(name %p = 0x7ffeeaf58730 ...)
name	/tmp/foo.png                                                                                                                                                                                            @
name_c	/tmp/foo.png
RETURN oct_gdimage_create_from() = 0x20e2fc0
 Trying file '/tmp/foo.png' for BoxShape = box_image.
 Done.
make[1]: Leaving directory `/home/SOFT/octopus/fortran-c-interop'
make intel=1 sep clean oct
make[1]: Entering directory `/home/SOFT/octopus/fortran-c-interop'

--------------------------------

rm -f *.o *.mod a.out oct demo
ifort -std03 -c simul_box.F90
icc -I.   -c -o oct_gdlib_f.o oct_gdlib_f.c

--------------------------------
Octopus isolated code, ifort
--------------------------------

ifort --version
ifort (IFORT) 19.0.5.281 20190815
Copyright (C) 1985-2019 Intel Corporation.  All rights reserved.

ifort -o oct simul_box.o oct_gdlib_f.o
./oct
 Trying file '/tmp/foo.png' for BoxShape = box_image.
forrtl: severe (174): SIGSEGV, segmentation fault occurred
Image              PC                Routine            Line        Source             
oct                0000000000404953  Unknown               Unknown  Unknown
libpthread-2.17.s  00002AB208FB85D0  Unknown               Unknown  Unknown
oct                0000000000403A0D  Unknown               Unknown  Unknown
oct                0000000000403961  Unknown               Unknown  Unknown
oct                0000000000403822  Unknown               Unknown  Unknown
libc-2.17.so       00002AB2091E7495  __libc_start_main     Unknown  Unknown
oct                0000000000403729  Unknown               Unknown  Unknown
ENTER oct_gdimage_create_from(name %p = 0x7ffcb36713b8 ...)
name	hjÊ²*
make[1]: *** [oct] Error 174
make[1]: Leaving directory `/home/SOFT/octopus/fortran-c-interop'
make: [all-oct] Error 2 (ignored)
```

### Demo Code

```
$ make all-demo
```
Output:
```
make gnu=1   sep clean demo
make[1]: Entering directory `/home/SOFT/octopus/fortran-c-interop'

--------------------------------

rm -f *.o *.mod a.out oct demo
gfortran -std=f2003 -c intel-demo-f.F90
gcc -I.   -c -o intel-demo-c.o intel-demo-c.c

--------------------------------
Demo code, gfortran
--------------------------------

gfortran -o demo intel-demo-f.o intel-demo-c.o
./demo
 Fortran interoperates with C
make[1]: Leaving directory `/home/SOFT/octopus/fortran-c-interop'
make intel=1 sep clean demo
make[1]: Entering directory `/home/SOFT/octopus/fortran-c-interop'

--------------------------------

rm -f *.o *.mod a.out oct demo
ifort -std03 -c intel-demo-f.F90
icc -I.   -c -o intel-demo-c.o intel-demo-c.c

--------------------------------
Demo code, ifort
--------------------------------

ifort -o demo intel-demo-f.o intel-demo-c.o
./demo
 Fortran interoperates with C
 Fortran interoperates with C
make[1]: Leaving directory `/home/SOFT/octopus/fortran-c-interop'
```

The demo code shows _two_ ways of accessing a string passed from C to Fortran.
Notice how, unfortunately, the first of these fails using gfortran.
